package modbus_gateway

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"math"
	"reflect"
	"time"

	"github.com/influxdata/telegraf"
	"github.com/influxdata/telegraf/metric"
	"github.com/prometheus/common/log"
)

func (m *ModbusGateway) Gather(acc telegraf.Accumulator) error {
	if !m.isConnected {
		m.Log.Debugf("Connecting to %s", m.Gateway)
		err := connect(m)
		if err != nil {
			m.isConnected = false
			return err
		} else {
			m.Log.Debug("Connection successful")
		}
	}

	grouper := metric.NewSeriesGrouper()

	for _, req := range m.Requests {
		now := time.Now()
		m.tcpHandler.SetSlave(req.Unit)
		//m.tcpHandler.SlaveId = req.Unit
		var resp []byte
		var err error

		if req.RequestType == "holding" {
			resp, err = m.client.ReadHoldingRegisters(req.Address, req.Count)
		} else if req.RequestType == "input" {
			resp, err = m.client.ReadInputRegisters(req.Address, req.Count)
		} else if req.RequestType == "discrete" {
			resp, err = m.client.ReadDiscreteInputs(req.Address, req.Count)
		} else {
			return fmt.Errorf("Don't know how to poll register type \"%s\"", req.RequestType)
		}

		if err == nil {
			reader := bytes.NewReader(resp)

			for _, f := range req.Fields {
				/*
				 * Look up the byte ordering for this request, with higher level overrides
				 */
				var orderSpec string
				if f.Order != "" {
					orderSpec = f.Order
				} else if req.Order != "" {
					orderSpec = req.Order
				} else if m.Order != "" {
					orderSpec = m.Order
				} else {
					orderSpec = "ABCD"
				}

				byteOrder, wasNew := getOrCreateByteOrder(orderSpec)
				if wasNew {
					/* First time this byte ordering has been used, dump its configuration */
					m.Log.Debugf("Created a new byte order %s with ordering %v", byteOrder.order, byteOrder.positions)
				}

				switch f.InputType {
				case "UINT16":
					var value uint16
					binary.Read(reader, byteOrder, &value)
					if f.Omit == false {
						grouper.Add(req.MeasurementName, nil, now, f.Name, scale(&f, value))
					}
					break
				case "INT16":
					var value int16
					binary.Read(reader, byteOrder, &value)
					if f.Omit == false {
						grouper.Add(req.MeasurementName, nil, now, f.Name, scale(&f, value))
					}
					break
				case "UINT32":
					var value uint32
					binary.Read(reader, byteOrder, &value)
					if f.Omit == false {
						grouper.Add(req.MeasurementName, nil, now, f.Name, scale(&f, value))
					}
					break
				case "INT32":
					var value int32
					binary.Read(reader, byteOrder, &value)
					if f.Omit == false {
						grouper.Add(req.MeasurementName, nil, now, f.Name, scale(&f, value))
					}
					break

				case "FLOAT32", "FLOAT32-IEEE":
					var value float32
					binary.Read(reader, byteOrder, &value)
					if f.Omit == false {
						grouper.Add(req.MeasurementName, nil, now, f.Name, scale(&f, value))
					}
					break
				default:
					m.Log.Warnf("Invalid conversion type %s", f.InputType)
				}
			}
		} else {
			m.Log.Warn("Modbus Error: ", err)
			m.tcpHandler.Close()
			m.tcpHandler.Connect()
		}
	}

	for _, metric := range grouper.Metrics() {
		m.Log.Infof("write %v", metric)
		acc.AddMetric(metric)
	}

	return nil
}

func scale(f *FieldDef, value interface{}) interface{} {
	switch f.OutputFormat {
	case "FLOAT", "FLOAT64":
		switch v := value.(type) {
		case int:
			return int((float64(v) * f.Scale) + f.Offset)
		case int16:
			return int16((float64(v) * f.Scale) + f.Offset)
		case uint16:
			return uint16((float64(v) * f.Scale) + f.Offset)
		case int32:
			return int32((float64(v) * f.Scale) + f.Offset)
		case uint32:
			return uint32((float64(v) * f.Scale) + f.Offset)
		case float32:
			return float32((float64(v) * f.Scale) + f.Offset)
		case float64:
			return (float64(v) * f.Scale) + f.Offset
		default:
			return nil
		}

	case "FLOAT32":
		switch v := value.(type) {
		case int:
			return float32((float64(v) * f.Scale) + f.Offset)
		case int16:
			return float32((float64(v) * f.Scale) + f.Offset)
		case uint16:
			return float32((float64(v) * f.Scale) + f.Offset)
		case int32:
			return float32((float64(v) * f.Scale) + f.Offset)
		case uint32:
			return float32((float64(v) * f.Scale) + f.Offset)
		default:
			return nil
		}

	case "INT", "INT32", "INT64":
		switch v := value.(type) {
		case int:
			return int64(math.Round((float64(v) * f.Scale) + f.Offset))
		case int16:
			return int64(math.Round((float64(v) * f.Scale) + f.Offset))
		case uint16:
			return int64(math.Round((float64(v) * f.Scale) + f.Offset))
		case int32:
			return int64(math.Round((float64(v) * f.Scale) + f.Offset))
		case uint32:
			return int64(math.Round((float64(v) * f.Scale) + f.Offset))
		default:
			return nil
		}

	case "UINT", "UINT32", "UINT64":
		switch v := value.(type) {
		case int:
			return uint64(math.Round((float64(v) * f.Scale) + f.Offset))
		case int16:
			return uint64(math.Round((float64(v) * f.Scale) + f.Offset))
		case uint16:
			return uint64(math.Round((float64(v) * f.Scale) + f.Offset))
		case int32:
			return uint64(math.Round((float64(v) * f.Scale) + f.Offset))
		case uint32:
			return uint64(math.Round((float64(v) * f.Scale) + f.Offset))
		default:
			return nil
		}

	default:
		log.Warnf("Invalid output format %s", reflect.TypeOf(value))
		return nil
	}
}
